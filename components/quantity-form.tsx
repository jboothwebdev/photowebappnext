
import { Box, Flex, FormControl, FormLabel, Input } from '@chakra-ui/react';

export default function QuantityForm(props: any) {
  // TODO: validate no negative numbers
  return (
    <Flex>
      <form>
        <Flex
          className='price-form-inner'
          flexDir={'column'}
          justify={'center'}
          align={'center'}
          w='100%'
        >
          <Box w='40%'>
            <FormControl >
              <FormLabel htmlFor="quantity">Max Quantity</FormLabel>
              <Input
                id="quantity"
                type="number"
                name="quantity"
                value={props.value}
                onChange={props.handleChange}
              />
            </FormControl>
          </Box>
        </Flex>
      </form>
    </Flex>

  )

}

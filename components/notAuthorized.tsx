import { useRouter } from "next/router";
import Link from "next/link";
import { motion } from "framer-motion";
import { Flex, Heading, Text, Link as ChakraLink } from "@chakra-ui/react";

export const NotAuthorized = () => {
  return (
    <Flex h="35%" w="32%" justify="center" align="center">
      <motion.div
        initial={{ scale: 0 }}
        animate={{ scale: 1 }}
        transition={{ duration: 1 }}
      >
        <Flex
          flexDir="column"
          justify="center"
          align="center"
          bg="gray.50"
          borderRadius={"2xl"}
          h="30vh"
          w="32vw"
          p="2rem"
        >
          <Heading color="red.600" mb={2}>
            401: Not Authorized
          </Heading>
          <Text color="gray.700">
            You must be a registered employee to view this page please go back
            and
            <Link href="/login" passHref>
              <ChakraLink color="purple.600"> Login </ChakraLink>
            </Link>
          </Text>
        </Flex>
      </motion.div>
    </Flex>
  );
};

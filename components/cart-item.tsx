import { Box, Button, Flex, useColorModeValue } from "@chakra-ui/react";

const CartItem = ({ item, deleteItem }) => {
  const bg = useColorModeValue('gray.50', 'gray.50')

  return (
    <Flex
      flexDir={'row'}
      m={2}
      boxShadow='md'
      borderRadius={'md'}
      height={'10vh'}
      maxH={'15vh'} bg={bg}
      justify='center'
    >
      <Flex minW={"50%"} m={3} flexDir='row' justifyContent='space-evenly' align='center' fontWeight={'600'} fontSize={'lg'}>
        <Box>
          {item.Name}
        </Box>
        <Box>
          Qty: {item.Quantity}
        </Box>
        <Box>
          Format: {item.Medium}
        </Box>
      </Flex>
      <Flex minW={"33%"} m={3} flexDir='column' justify='flex-end' align='flex-end'>
        <Box>
          Item price: {item.Price}
        </Box>
        <Box>
          <Button color={'red.500'} onClick={() => deleteItem(item)}>Delete</Button>
        </Box>
      </Flex>
    </Flex>
  )
}

export default CartItem;

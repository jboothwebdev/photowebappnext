import { Box, Button, Flex, Heading, Modal, ModalBody, ModalCloseButton, ModalContent, ModalFooter, ModalHeader, ModalOverlay, Text, useColorModeValue, useDisclosure } from '@chakra-ui/react';
import { IItem } from '../models/IItem';
import { useContext } from 'react';
import useCart from '../state/cart.state';
import { useRouter } from 'next/router';
import { CustomerContext } from '../context/customer-context';
import { orderService } from '../services/order-service';

export default function CheckoutPreview() {
  const { customerState } = useContext(CustomerContext);
  const { isOpen, onOpen, onClose } = useDisclosure();
  const router = useRouter()
  const bg = useColorModeValue('gray.50', 'teal.50');
  const { cart } = useCart();
  let totalPrice: number = 0;


  function getTotalPrice() {
    let sum = 0;
    for (let item of cart) {
      sum += item.Price;
    }
    totalPrice = sum;
    console.log(cart)
  }


  function saveOrderToDb() {
    orderService.prepareOrder(cart).then(() => { router.push("/checkout") })


  }

  getTotalPrice();

  return (
    <Flex
      w={'25%'}
      h={'35%'}
      ml={4}
      bg={bg}
      color={'gray.700'}
      borderRadius={'md'}
      flexDir={'column'}
      boxShadow='dark-lg'
    >
      <Flex
        bgColor={'gray.50'}
        flexDir={'column'}
        justify={'center'}
        align={'center'}
        boxShadow='lg'
        borderRadius={'md'}
        mt={3}
        marginX={3}
      >
        <Flex
          flexDir={'column'}
          justify={'center'}
          mt={3}
          marginX={3}
        >
          <Heading size='md'>
            Customer information
          </Heading>
          <Text>
            {customerState.user.Name}
          </Text>

          <Text>
            {customerState.user.Email}
          </Text>
          <Text>
            {customerState.user.PhoneNumber}
          </Text>
          <Text>
            {customerState.user.Studio}
          </Text>
        </Flex>
        <Flex
          flexDir={'column'}
          justify={'center'}
          align={'center'}
          marginX={4}
          mt={5}
          pb={2}
        >
          <Heading size={'md'}> Cart Total: {totalPrice} </Heading>
          <Button
            mt={4}
            colorScheme='yellow'
            size='md'
            onClick={() => onOpen()}
          > Go to checkout</Button>
        </Flex>
      </Flex>
      <Modal isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>
            Confirm Order
            <ModalCloseButton />
            <ModalBody>
              You will be logged out and will not be able to change your order after this.
              If you need to make a change hit the close button.
            </ModalBody>
            <ModalFooter>
              <Button colorScheme="pink" mr={3} onClick={onClose}>
                Close
              </Button>
              <Button
                variant="ghost"
                color="teal.500"
                onClick={() => {
                  saveOrderToDb();
                  onClose();
                }}
              >
                Confirm
              </Button>
            </ModalFooter>
          </ModalHeader>
        </ModalContent>
      </Modal>
    </Flex>
  )
}

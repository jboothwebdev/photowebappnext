import {
  Box,
  Button,
  Flex,
  Icon,
  Menu,
  MenuButton,
  MenuItem,
  MenuList,
  Modal, ModalBody, ModalCloseButton,
  ModalContent, ModalFooter, ModalHeader,
  ModalOverlay,
  useColorMode,
  useColorModeValue,
} from '@chakra-ui/react';
import { MoonIcon, SunIcon } from "@chakra-ui/icons";
import { useRouter } from 'next/router';
import { MdShoppingCart } from 'react-icons/md'

const CustomerNavbar = () => {
  const { colorMode, toggleColorMode } = useColorMode();
  const bg = useColorModeValue("blue.500", "gray.700");
  const color = useColorModeValue("gray.900", "gray.100");
  const router = useRouter();

  return (
    <>
      <Flex
        position="absolute"
        top="0"
        h="4em"
        w="100vw"
        flexDir="row"
        justify="space-between"
        bg={bg}
        color={color}
        p={3}
        borderRadius="lg"
        zIndex="9999"
      >
        <Button onClick={() => router.push('/')}
        >
          O.M.P
        </Button>
        <Box>
          <Button mr={2} onClick={() => { router.push("/cart") }}>
            <Icon as={MdShoppingCart} />
          </Button>
          <Button onClick={toggleColorMode} mr={2} >
            {colorMode === "light" ? <MoonIcon /> : <SunIcon />}
          </Button>
        </Box>
      </Flex>
    </>
  )
}

export default CustomerNavbar

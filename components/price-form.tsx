import { Box, Button, Flex, FormControl, FormLabel, Input } from '@chakra-ui/react';

export default function PriceForm(props: any) {

  return (
    <Flex className='outer'
    >
      <Flex className='price-form'
      >
        <form>
          <Flex
            className='price-form-inner'
            flexDir={'column'}
            w='100%'
            align={'center'}
            justify={'center'}
          >
            <Box
              w='39%'
            >
              <FormControl >
                <FormLabel htmlFor="print">print</FormLabel>
                <Input
                  id="print"
                  type="number"
                  name="print"
                  value={props.value.print}
                  onChange={props.handleChange}
                />
              </FormControl>
              <FormControl >
                <FormLabel htmlFor="digital">digital</FormLabel>
                <Input
                  id="digital"
                  type="number"
                  name="digital"
                  value={props.value.digital}
                  onChange={props.handleChange}
                />
              </FormControl>
              <FormControl >
                <FormLabel htmlFor="combo">combo</FormLabel>
                <Input
                  id="combo"
                  type="number"
                  name="combo"
                  value={props.value.combo}
                  onChange={props.handleChange}
                />
              </FormControl>
            </Box>
          </Flex>
        </form>
      </Flex>
    </Flex>
  )
}   

import {
  Flex,
  Heading,
  Table,
  TableContainer,
  Thead,
  Tbody,
  Tr,
  Th,
  Td,
  Button
} from '@chakra-ui/react';
import { useRouter } from 'next/router';
import { ICustomer } from '../models/ICustomer';
import { customerService } from '../services/customer-service';

export default function CustomerList({ customer, onDelete }) {
  const router = useRouter();

  function deleteCustomer(customer: ICustomer) {
    customerService.deleteCustomer(customer).then(onDelete())
  }

  return (
    <Flex
      flexDir={'column'}
      ml={'20vw'}
    >
      <Heading size='lg'>
        Current Customers
      </Heading>
      <TableContainer>
        <Table variant='simple' colorScheme={'whiteAlpha'}>
          <Thead>
            <Tr>
              <Th> Name</Th>
              <Th> Email</Th>
              <Th> Phonenumber</Th>
              <Th> Studio</Th>
              <Th> </Th>
            </Tr>
          </Thead>
          <Tbody cursor='pointer'>
            {customer ? customer.map((item: ICustomer) => (
              <Tr key={item.id} h={'5vh'} >
                <Td onClick={() => { router.push(`/customers/${item.Email}`) }}>{item.Name}</Td>
                <Td onClick={() => { router.push(`/customers/${item.Email}`) }}>{item.Email}</Td>
                <Td onClick={() => { router.push(`/customers/${item.Email}`) }}>{item.PhoneNumber}</Td>
                <Td onClick={() => { router.push(`/customers/${item.Email}`) }}>{item.Studio}</Td>
                <Td><Button variant='link' colorScheme={'red'} onClick={() => { deleteCustomer(item) }}>delete</Button></Td>
              </Tr>
            )) : <Tr><Td> no customer</Td></Tr>}
          </Tbody>

        </Table>
      </TableContainer>
    </Flex>
  )
}

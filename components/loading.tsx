import {Flex, Heading} from '@chakra-ui/react';

export default function Loading(){
    return(
      <Flex>
        <Heading size={'md'} fontSize={8}>
          Loading
        </Heading>
      </Flex>
    )
}
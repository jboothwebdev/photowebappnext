import {
  Flex,
  Heading,
  Table,
  TableContainer,
  Thead,
  Tbody,
  Tr,
  Th,
  Td,
  Button
} from '@chakra-ui/react';
import { useRouter } from 'next/router';
import { IItem } from '../models/IItem';
import { orderService } from '../services/order-service';

export default function Orderlist({ orders, sendDelete }) {

  const router = useRouter()

  function deleteOrder(item: IItem) {
    orderService.deleteOrder(item)
    sendDelete()
  }

  return (
    <Flex
      flexDir={'column'}
      ml={'20vw'}
    >
      <Heading size='lg'>
        Current Orders
      </Heading>
      <TableContainer>
        <Table variant='simple' colorScheme={'whiteAlpha'}>
          <Thead>
            <Tr>
              <Th> Email</Th>
              <Th> Photo Name</Th>
              <Th> Format</Th>
              <Th> Quantity</Th>
              <Th></Th>
              <Th></Th>
            </Tr>
          </Thead>
          <Tbody>
            {orders ? orders.map((item: IItem) => (
              <Tr key={item.id} h={'5vh'}>
                <Td>{item.CustomerEmail}</Td>
                <Td>{item.Name}</Td>
                <Td>{item.Medium}</Td>
                <Td>{item.Quantity}</Td>
                <Td><Button
                  variant='link'
                  colorScheme={'yellow'}
                  onClick={() => { router.push(`/order/${item.id}`) }}
                  mt={'2.5vh'}>Edit</Button></Td>
                <Td><Button variant='link' colorScheme={'red'} mt={'2.5vh'} onClick={() => deleteOrder(item)}> delete</Button></Td>
              </Tr>
            )) : <Tr><Td> no orders</Td></Tr>}
          </Tbody>
        </Table>
      </TableContainer>
    </Flex >
  )
}

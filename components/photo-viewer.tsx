import { useState } from 'react'
import {
  SimpleGrid,
  Flex,
  Box,
  Image,
  Modal,
  ModalBody,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalOverlay,
  ModalCloseButton,
  useDisclosure
} from '@chakra-ui/react';
import { IEncodedPhoto } from '../models/IEncodedPhoto';
import PhotoSelectionForm from './photo-selection-form';

const PhotoViewer = (props: any) => {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [selectedPhoto, setSelectedPhoto] = useState<IEncodedPhoto>({
    name: '',
    data: ''
  })
  let formatedPhotos: any = []

  const formatPhotos = () => {
    props.unformatedPhotos.map((photo: IEncodedPhoto, i: number) => {
      formatedPhotos.push(
        <Box maxW='md' width='auto' h='auto' pos='relative' overflow='hidden' key={i}>
          <Image key={photo.name + i} src={photo.data} alt={photo.name} objectFit='contain' width={'auto'} h={'auto'} htmlWidth='30vw' htmlHeight='auto' maxH={'400px'} onClick={() => { openSelectedPhoto(photo) }} />
        </Box >
      )
    })
  }

  const openSelectedPhoto = (photo: IEncodedPhoto) => {
    setSelectedPhoto(photo)
    onOpen();
  }

  formatPhotos()
  return (
    <>
      <Flex
        position="absolute"
        top={'100px'}
        height={'auto'}
        width={'100%'}
        flexDir='column'
        justify={'center'}
      >
        <SimpleGrid minChildWidth='300px' spacing='1px'>
          {formatedPhotos}
        </SimpleGrid>
      </Flex>
      <Modal isOpen={isOpen} onClose={onClose} size={'6xl'} >
        <ModalOverlay />
        <ModalContent data-testid='photoModal' top='1vh' minH='50%' maxH='90%' maxW='100%' w='100%'>
          <ModalHeader>
            <ModalCloseButton />
            <ModalBody h='80%' w='95%' maxH='fit-content'>
              <Flex maxH='90%' justify='space-between'>
                <Box maxH='100%' width='100%'>
                  <Image src={selectedPhoto.data} alt={selectedPhoto.name} objectFit="scale-down" boxSize='100%' maxH='100vh' />
                </Box>
                <Box>
                  <PhotoSelectionForm sendOnClose={onClose} photo={selectedPhoto} />
                </Box>
              </Flex>
            </ModalBody>
            <ModalFooter>
            </ModalFooter>
          </ModalHeader>
        </ModalContent>
      </Modal>
    </>
  )
}

export default PhotoViewer; 

import {
  Flex,
  Button,
  Box,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  useColorMode,
  useColorModeValue,
  useDisclosure,
} from "@chakra-ui/react";
import { Menu, MenuButton, MenuList, MenuItem } from "@chakra-ui/react";
import { SunIcon, MoonIcon, HamburgerIcon } from "@chakra-ui/icons";
import { useRouter } from "next/router";
import { useContext } from "react";
import { UserContext } from "../context/user-context";

const Navbar = () => {
  const { colorMode, toggleColorMode } = useColorMode();
  const { isOpen, onOpen, onClose } = useDisclosure();
  const { state, dispatch } = useContext(UserContext);
  const router = useRouter();
  const bg = useColorModeValue("blue.500", "gray.700");
  const color = useColorModeValue("gray.900", "gray.100");
  const sendLogoutRequest = async () => {
    await dispatch({ type: "CLEAR_USER" });
    router.push("/");
  };
  return (
    <Flex
      position="absolute"
      top="0"
      h="4em"
      w="100%"
      flexDir="row"
      justify="space-between"
      bg={bg}
      color={color}
      p={3}
      borderRadius="lg"
      zIndex="9999"
    >
      <Button
        onClick={() => {
          router.push("/");
        }}
      >
        OMP
      </Button>
      <Box>
        <Button onClick={toggleColorMode} mr={2}>
          {colorMode === "light" ? <MoonIcon /> : <SunIcon />}
        </Button>
        <Menu>
          <MenuButton
            bg={"whiteAlpha.200"}
            h="2.5rem"
            w="2.7rem"
            mr={2}
            pb={1}
            borderRadius="md"
          >
            <HamburgerIcon />
          </MenuButton>
          {state.user !== "" ? (
            <MenuList>
              <MenuItem onClick={onOpen}> Logout</MenuItem>
            </MenuList>
          ) : (
            <MenuList>
              <MenuItem
                onClick={() => {
                  router.push("/login");
                }}
              >
                Login
              </MenuItem>
            </MenuList>
          )}
        </Menu>
      </Box>
      <Modal isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>Confirm</ModalHeader>
          <ModalCloseButton />
          <ModalBody>
            If you wish to logout please hit the confirm button below. Or hit
            cancel to go back.
          </ModalBody>

          <ModalFooter>
            <Button colorScheme="pink" mr={3} onClick={onClose}>
              Close
            </Button>
            <Button
              variant="ghost"
              color="teal.500"
              onClick={() => {
                sendLogoutRequest();
                onClose();
              }}
            >
              Confirm
            </Button>
          </ModalFooter>
        </ModalContent>
      </Modal>
    </Flex>
  );
};

export default Navbar;

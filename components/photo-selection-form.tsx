import {
  Button,
  Flex,
  FormControl,
  FormLabel,
  Select,
  Radio,
  RadioGroup
} from "@chakra-ui/react";
import { ChangeEvent, useContext, useEffect, useState } from "react";
import { ItemContext } from "../context/item-context";
import { CustomerContext } from '../context/customer-context';
import useCart from '../state/cart.state';
import usePrices from "../state/prices.state";
import useQuantity from "../state/quantity.state";
import { ReactJSXElement } from "@emotion/react/types/jsx-namespace";

const PhotoSelectionForm = (props) => {
  const { customerState } = useContext(CustomerContext);
  const { itemDispatch } = useContext(ItemContext);
  const { cart, setCart } = useCart()
  const { prices } = usePrices();
  const [formatValue, setFormValue] = useState<string>('');
  const [quantityValue, setQuantityValue] = useState('1');
  const { quantity } = useQuantity();
  const [options, setOptions] = useState<Array<ReactJSXElement>>([]);


  let savedItems: Array<any> = [];

  useEffect(() => {
    createQuantityOptions();
  }, [])

  function calculatePrice(format: string, value: string) {
    switch (format) {
      case 'print':
        return prices.print * parseInt(value)
      case 'digital':
        return prices.digital * parseInt(value)
      case 'combo':
        return prices.combo * parseInt(value)
      default:
        return 0;
    }
  }

  const handleRadioChange = (event: string) => {
    setFormValue(event)
  }

  const handleQuantityChange = (event: ChangeEvent<HTMLSelectElement>) => {
    setQuantityValue(event.target.value)
  }

  const handleSubmit = (event: any) => {
    event.preventDefault();
    let calculatedPrice = calculatePrice(formatValue, quantityValue);
    let values = {
      format: formatValue,
      quantity: quantityValue,
      price: calculatedPrice
    }
    addItemToState(values)
    props.sendOnClose()
  }

  const addItemToState = (values: any) => {
    const payload = {
      Name: props.photo.name,
      CustomerEmail: customerState.user.Email,
      Quantity: values.quantity,
      Medium: values.format,
      Price: values.price
    }
    if (cart?.length > 0) {
      savedItems = cart;
    }
    savedItems.push(payload)
    setCart(savedItems)
    itemDispatch({ type: "SET_ITEM", payload: payload })
  }

  const createQuantityOptions = () => {
    let createdOptions: Array<ReactJSXElement> = [];
    for (let i = 1; i <= quantity; i++) {
      createdOptions.push(
        <option key={i} value={i} >{i}</option >
      )
    }
    setOptions(createdOptions)
  }

  return (
    <Flex className="photo-form" ml={2}>
      <form onSubmit={handleSubmit}>
        <Flex flexDir="column" justify='center' align='center'>
          <Flex flexDir='column' justify='center' align='center' w='100%'>
            <FormControl isRequired w='100%' >
              <FormLabel mt='2'>Format</FormLabel>
              <RadioGroup onChange={handleRadioChange} value={formatValue} mt='3' display='flex' flexDir='column'>
                <Radio value='print'>Print: ${prices.print}</Radio>
                <Radio value='digital'>Digital: ${prices.digital}</Radio>
                <Radio value='combo'>Prints with Digital copies: ${prices.combo}</Radio>
              </RadioGroup>
            </FormControl>
            <FormControl isRequired w='100%'>
              <FormLabel mt='2'>Quantity</FormLabel>
              <Select onChange={handleQuantityChange} value={quantityValue} mt='3'>
                {options}
              </Select>
            </FormControl>
          </Flex>
          <Flex mt='3'>
            <Button onClick={props.sendOnClose} colorScheme='pink' mr='2'>Cancel</Button>

            <Button type='submit' colorScheme='teal' ml='2'>Add to cart</Button>
          </Flex>
        </Flex>
      </form>
    </Flex >
  )
}

export default PhotoSelectionForm;


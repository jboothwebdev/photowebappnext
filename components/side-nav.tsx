import {
  Button,
  Flex,
  Heading,
  Input,
  Modal,
  ModalOverlay,
  ModalContent,
  ModalHeader,
  ModalFooter,
  ModalBody,
  ModalCloseButton,
  useDisclosure
} from "@chakra-ui/react";
import Link from "next/link";
import { BaseSyntheticEvent } from "react";
import { useContext } from "react";
import { PhotoContext } from "../context/photo-context";
import { UserContext } from "../context/user-context";
import { useRouter } from "next/router";

const SideNav = () => {
  let selections: any = [];
  let loading = false;
  const router = useRouter();
  const { photoState, photoDispatch } = useContext(PhotoContext);
  const { state, dispatch } = useContext(UserContext)
  const { isOpen, onOpen, onClose } = useDisclosure()


  const addSelectionToState = (event: BaseSyntheticEvent) => {
    loading = true;
    for (let i = 0; i < event.target.files.length; i++) {
      const reader = new FileReader();
      let result = {
        name: event.target.files[i].name,
        data: ''
      }
      reader.readAsDataURL(event.target.files[i]);
      reader.onload = function(e: any) {
        result.data = e.target.result;
      }
      setTimeout(() => {
        selections.push(result);
        if (selections.length === event.target.files.length) {
          photoDispatch({ type: "SET_PHOTO", payload: selections });
          loading = false;
        }
      }, 300)
    }
  }

  const startUpload = () => {
    document.getElementById("file-input").click();
  };

  const switchToCustomerPortal = () => {
    router.push('/customer')
  }

  const routeToOrders = () => {
    router.push('/orders')
  }

  return (
    <Flex
      width="20%"
      height="100%"
      bg={'gray.50'}
      position="absolute"
      left="0"
      top="4em"
      zIndex="100"
      flexDir="column"
      borderRight="1px"
      borderColor={"gray.50"}
    >
      <Heading color={'gray.700'} fontSize={"1rem"} m={2}>
        Employee Options
      </Heading>
      <Input
        display="none"
        type="file"
        className="file-input"
        id="file-input"
        //@ts-ignore
        webkitdirectory="true"
        onChange={(event) => {
          addSelectionToState(event);
        }}
      ></Input>
      <Button
        m={2}
        variant='ghost'
        color={'blue.600'}
        _hover={{ color: 'blue.400', bg: 'gray.200' }}
        onClick={() => {
          startUpload();
        }}
      >
        Upload Photos
      </Button>
      <Link href="/item-config" passHref>
        <Button
          variant='ghost'
          color={'blue.600'}
          _hover={{ color: 'blue.400', bg: 'gray.200' }}
          m={2}
        >
          Selections Config
        </Button>
      </Link>

      <Button
        variant='ghost'
        color={'blue.600'}
        _hover={{ color: 'blue.400', bg: 'gray.200' }}
        m={2}
        onClick={() => { routeToOrders() }}
      >
        Current Orders
      </Button>

      <Button
        variant='ghost'
        color={'blue.600'}
        _hover={{ color: 'blue.400', bg: 'gray.200' }}
        m={2}
        onClick={() => { router.push('/current-customers') }}
      >
        Current Customers
      </Button>
      {loading ? "loading" :
        <Button
          m={2}
          variant='ghost'
          color={'blue.600'}
          borderRadius={'0px'}
          borderTop={'1px'}
          _hover={{ color: 'blue.400', bg: 'gray.200' }}
          onClick={onOpen}>Customer Portal</Button>
      }
      <Button
        m={2}
        variant='ghost'
        color={'red.500'}
        borderRadius={'0px'}
        borderTop={'1px'}
        _hover={{ color: 'red.800', bg: 'gray.200' }}
        onClick={() => { router.push('/confirm-delete') }}>
        Reset Session
      </Button>
      <Modal isOpen={isOpen} onClose={onClose}>
        <ModalOverlay />
        <ModalContent>
          <ModalHeader>
            Confirm Enter Customer Portal
            <ModalCloseButton />
            <ModalBody>
              This action will log you out and send you to the customer portal. If you need to make any changes
              you will have to exit to the home page and log back in.
            </ModalBody>
            <ModalFooter>
              <Button colorScheme="pink" mr={3} onClick={onClose}>
                Close
              </Button>
              <Button
                variant="ghost"
                color="teal.500"
                onClick={() => {
                  switchToCustomerPortal()
                  onClose();
                }}
              >
                Confirm
              </Button>
            </ModalFooter>
          </ModalHeader>
        </ModalContent>
      </Modal>
    </Flex >
  );
};

export default SideNav;

import type { NextPage } from "next";
import { Alert, AlertIcon, AlertTitle, AlertDescription, Flex, CloseButton } from "@chakra-ui/react";
import Navbar from "../components/navbar";
import useForm from "../hooks/useForm";
import { useRouter } from "next/router";
import { LoginService } from "../services/login-service";
import { useContext, useState } from "react";
import { UserContext } from "../context/user-context";
import { Iemployee } from "../models/IEmployee";
import {
  Button,
  Input,
  FormControl,
  FormLabel,
  FormErrorIcon,
  FormErrorMessage,
} from "@chakra-ui/react";

const Login: NextPage = () => {
  const [loginError, setLoginError] = useState(false)
  const { values, handleChange, handleSubmit } = useForm(() => {
    processLogin(values);
  });
  const { state, dispatch } = useContext(UserContext);
  const router = useRouter();
  //@ts-ignore
  const checkForValidName = values.username === "";
  //@ts-ignore
  const checkForValidPassword = values.password === "";

  //TODO: make this useable with the model class
  const checkForValidUser = (user: any) => {
    if (!user.Username) {
      setLoginError(true)
      return null;
    }
    dispatch({ type: "SET_USER", payload: user.Username });
    router.push("/employee");
  };

  const processLogin = async (values: any) => {
    const loginResponse = await LoginService.getEmployee(
      values.username,
      values.password
    );
    checkForValidUser(loginResponse);
  };

  return (
    <Flex
      h="100vh"
      className="login-container"
      flexDir="column"
      justify="center"
      align="center"
    >
      <Navbar />
      <Flex mt="5em" flexDir={'column'} justify={'center'} >
        {loginError &&
          <Alert status='error' borderRadius={'md'} colorScheme={'red'} bg={'rgba(255, 90, 100, 0.6)'} mb={10}>
            <AlertIcon />
            <AlertTitle>
              Loging Error!
            </AlertTitle>
            <AlertDescription>
              Your username or password was invalid please try again.
            </AlertDescription>
            <CloseButton onClick={() => { setLoginError(false) }} />
          </Alert>}
        <Flex justify={'center'}>
          <form onSubmit={handleSubmit}>
            <FormControl isRequired isInvalid={checkForValidName}>
              <FormLabel htmlFor="username">Name</FormLabel>
              <Input
                id="username"
                type="username"
                name="username"
                value={values.username}
                onChange={handleChange}
              />
              <FormErrorMessage>
                <FormErrorIcon />
                Name is required
              </FormErrorMessage>
            </FormControl>
            <FormControl isRequired isInvalid={checkForValidPassword}>
              <FormLabel htmlFor="password" mt={3}>
                Password
              </FormLabel>
              <Input
                id="password"
                type="password"
                name="password"
                value={values.password}
                onChange={handleChange}
              />
              <FormErrorMessage>
                <FormErrorIcon />
                Email is required
              </FormErrorMessage>
            </FormControl>
            <Flex flexDir="row" justify="space-around">
              <Button mt={4} colorScheme="teal" type="submit">
                Submit
              </Button>
              <Button
                mt={4}
                colorScheme="pink"
                type="button"
                onClick={() => {
                  router.push("/");
                }}
              >
                Cancel
              </Button>
            </Flex>
          </form>
        </Flex>
      </Flex>
    </Flex>
  );
};

export default Login;

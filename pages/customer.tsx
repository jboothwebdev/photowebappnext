import { Box, Flex } from '@chakra-ui/react'
import { useContext, useEffect, useState } from 'react';
import { PhotoContext } from '../context/photo-context';
import CustomerNavbar from "../components/cust-navbar";
import CustomerSignup from './customer-signup';
import PhotoViewer from '../components/photo-viewer';
import { CustomerContext } from '../context/customer-context';
import { UserContext } from '../context/user-context';

// TODO: Add the user sign up form 
const Customer = () => {
  const [customerSignedUp, setCustomerSignedUp] = useState<boolean>(false)
  const { photoState } = useContext(PhotoContext);
  const { customerState } = useContext(CustomerContext)
  const { state, dispatch } = useContext(UserContext)
  let unformatedPhotos: any = [];
  useEffect(() => {
    checkforUser();
    dispatch({ type: 'CLEAR_USER' })
  }, [])

  const changeCustomerSignedUp = () => {
    setCustomerSignedUp(true)
  }

  const checkforUser = () => {
    if (customerState.user.name !== '') {
      setCustomerSignedUp(true);
    }
  }

  const addPhotosToArray = () => {
    unformatedPhotos = photoState.photos
  }

  addPhotosToArray()

  return (
    <Flex>
      <CustomerNavbar />
      {!customerSignedUp && <CustomerSignup changeCustomerSignedUp={changeCustomerSignedUp} />}
      {customerSignedUp && <PhotoViewer unformatedPhotos={unformatedPhotos} />}
    </Flex>
  )
}

export default Customer;

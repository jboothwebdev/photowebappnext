import {
  Button,
  Flex,
  Heading,
  Text,
  AlertDialog,
  AlertDialogBody,
  AlertDialogFooter,
  AlertDialogHeader,
  AlertDialogContent,
  AlertDialogOverlay,
  useDisclosure,
} from "@chakra-ui/react";
import { useRouter } from "next/router";
import * as React from 'react'
import { deleteService } from "../services/delete-service";

export default function ConfirmDelete() {
  const router = useRouter()
  const { isOpen, onOpen, onClose } = useDisclosure()
  const cancelRef = React.useRef()

  function sendDelete() {
    deleteService.deleteAll().then(() => { onOpen() })
  }

  return (
    <Flex
      flexDir={'column'}
      justify={'space-evenly'}
      align={'center'}
      minH={'50vh'}
    >
      <Heading>
        Warning !
      </Heading>
      <Text fontSize={'xl'}>
        This action will delete all the current Customers
        and all orders if you are sure hit confirm to continue
      </Text>
      <Flex justify={'space-around'} minW={'30vw'}>
        <Button colorScheme={'pink'} onClick={() => { sendDelete() }}>
          Confirm
        </Button>
        <Button colorScheme={'blue'} onClick={() => { router.push('/employee') }}>
          Cancel
        </Button>
      </Flex>
      <AlertDialog
        isOpen={isOpen}
        leastDestructiveRef={cancelRef}
        onClose={onClose}
        isCentered
      >
        <AlertDialogOverlay>
          <AlertDialogContent>
            <AlertDialogHeader fontSize='lg' fontWeight='bold'>
              Deletion Confirmed
            </AlertDialogHeader>

            <AlertDialogBody>
              Session has been reset there should be no current orders or customers
            </AlertDialogBody>

            <AlertDialogFooter>
              <Button onClick={() => { router.push('/employee') }}>
                Close
              </Button>
            </AlertDialogFooter>
          </AlertDialogContent>
        </AlertDialogOverlay>
      </AlertDialog>
    </Flex>
  )
}

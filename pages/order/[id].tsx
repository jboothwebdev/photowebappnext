import { Box, Button, Flex, FormControl, FormLabel, Heading, Input, Text } from '@chakra-ui/react'
import { Global } from '../../constants/constants';
import { useRouter } from 'next/router'
import { BaseSyntheticEvent, ChangeEvent, useEffect, useState } from 'react';
import useSWR from 'swr';
import { IItem } from '../../models/IItem';
import { orderService } from '../../services/order-service';
import Navbar from '../../components/navbar';
import SideNav from '../../components/side-nav';


export default function OrderDetails() {
  const router = useRouter();
  const { id } = router.query
  let API: string
  const [loading, setLoading] = useState(false)
  const [data, setData] = useState<any>({})
  const [body, setBody] = useState<IItem>({
    Name: data.Name,
    CustomerEmail: data.CustomerEmail,
    Quantity: data.Quantity,
    Medium: data.Medium,
    Price: data.Price

  })

  useEffect(() => {
    setId()
      .then((res) => {
        setData(res);
      }
      )
      .then(() => {
        setLoading(true);
      }
      )
  }, [])

  async function setId() {
    let url = `${Global.API_URL}/items/${id}`
    return fetch(url).then((res) => res.json())
  }

  function handleChange(event: ChangeEvent<HTMLInputElement>) {
    event.persist();
    setBody((values) => ({
      ...values,
      [event.target.name]: event.target.value
    }))
  }

  function handleSubmit(event: BaseSyntheticEvent) {
    event.preventDefault();
    orderService.updateOrder(body, id).then(() => {
      router.push('/orders')
    })
  }

  return (
    <Flex
      minH={'50vh'}
      flexDir={'row'}
      justify={'space-around'}
      align={'center'}
    >
      <Navbar />
      <SideNav />
      {loading ?
        <Flex
          minH={'50vh'}
          minW={'90vw'}
          mt={'25%'}
          justify={'space-around'}>

          <Flex
            ml={'25%'}
            flexDir={'column'}
          >
            <Heading size={'lg'}>
              Current Order Info
            </Heading>
            <Text>
              Customer: {data.CustomerEmail}
            </Text>
            <Text>
              Photo name: {data.Name}
            </Text>
            <Text>
              Quantity: {data.Quantity}
            </Text>
            <Text>
              Format: {data.Medium}
            </Text>
            <Text>
              Price: {data.Price}
            </Text>
          </Flex>
          <Flex
            flexDir={'column'}
          >
            <Heading size='md'>
              Change order
            </Heading>
            <form onSubmit={handleSubmit}>
              <Flex
                className='price-form-inner'
                flexDir={'column'}
                justify={'center'}
                align={'start'}
                w='100%'
              >
                <Box w='40%'>
                  <FormControl >
                    <FormLabel htmlFor="Quantity">Quantity</FormLabel>
                    <Input
                      id="Quantity"
                      type="number"
                      name="Quantity"
                      value={body.Quantity}
                      onChange={handleChange}
                    />
                  </FormControl>
                </Box>
                <Box w='40%'>
                  <FormControl >
                    <FormLabel htmlFor="Medium">Format</FormLabel>
                    <Input
                      id="Medium"
                      type="text"
                      name="Medium"
                      value={body.Medium}
                      onChange={handleChange}
                    />
                  </FormControl>
                </Box>
                <Box w='40%'>
                  <FormControl >
                    <FormLabel htmlFor="Price">Price</FormLabel>
                    <Input
                      id="Price"
                      type="number"
                      name="Price"
                      value={body.Price}
                      onChange={handleChange}
                    />
                  </FormControl>
                </Box>
                <Button mt={2} colorScheme={'teal'} type='submit'>Submit</Button>
              </Flex>
            </form>
          </Flex>
        </Flex>
        : <Text>loading</Text>}
    </Flex>
  )
}

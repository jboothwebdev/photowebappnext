import {
  Flex,
  Heading,
  Table,
  TableContainer,
  Thead,
  Tbody,
  Tr,
  Th,
  Td
} from '@chakra-ui/react';
import { useRouter } from 'next/router'
import { useEffect, useState } from 'react'
import { IItem } from '../../models/IItem';
import { customerService } from '../../services/customer-service'
import Navbar from '../../components/navbar'; import SideNav from '../../components/side-nav';
import useSWR from 'swr';
import { Global } from '../../constants/constants';
import axios from 'axios'

const fetcher = (url) => axios.post(url, body).then((res) => res.data)

const API = `${Global.API_URL}/customers-items`
let body = {}



export default function Customers() {
  const { data, error } = useSWR(setBody ? API : null, fetcher)
  const router = useRouter()
  const { email } = router.query
  useEffect(() => {
    setBody()
  }, [])

  function setBody() {
    body = {
      Email: email
    }
  }

  return (
    <Flex
      flexDir={'column'}
      h={'93vh'}
      justify={'space-around'}
      align={'center'}
    >
      <Navbar />
      <SideNav />
      <Flex
        flexDir={'column'}
        ml={'20vw'}
      >
        <Heading size='lg'>
          Oders for {email}
        </Heading>
        <TableContainer>
          <Table variant='simple' colorScheme={'whiteAlpha'}>
            <Thead>
              <Tr>
                <Th> Photo Name</Th>
                <Th> Format</Th>
                <Th> Quantity</Th>
              </Tr>
            </Thead>
            <Tbody cursor='pointer'>
              {data ? data.map((item: IItem) => (
                <Tr key={item.id} h={'5vh'}>
                  <Td>{item.Name}</Td>
                  <Td>{item.Medium}</Td>
                  <Td>{item.Quantity}</Td>
                </Tr>
              )) : <Tr><Td> no orders</Td></Tr>}
            </Tbody>
          </Table>
        </TableContainer>
      </Flex>
    </Flex>
  )
}

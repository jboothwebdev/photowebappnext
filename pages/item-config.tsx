import { Button, Flex } from '@chakra-ui/react';
import Navbar from '../components/navbar';
import SideNav from '../components/side-nav';
import PriceForm from '../components/price-form';
import QuantityForm from '../components/quantity-form';
import usePrices from '../state/prices.state';
import { ChangeEvent, useState } from 'react';
import { IPrices } from '../models/IPrices';
import useQuantity from '../state/quantity.state';



export default function Config() {
  const { setPrices } = usePrices();
  const [priceValue, setPriceValues] = useState<IPrices>({
    print: 0,
    digital: 0,
    combo: 0
  });

  const { setQuantity } = useQuantity();
  const [quantityValue, setQuantityValue] = useState<number>(1);

  function handlePriceChange(event: ChangeEvent<HTMLInputElement>) {
    event.persist();
    setPriceValues((values) => ({
      ...values,
      [event.target.name]: event.target.value
    }))
  }

  function handleQuantityChange(event: ChangeEvent<HTMLInputElement>) {
    event.persist();
    setQuantityValue(parseInt(event.target.value))
  }

  function handleSubmit() {
    setPrices(priceValue);
    setQuantity(quantityValue);

  }

  //TODO: add route guard 

  return (
    <Flex
      flexDir={'column'}
      h={'93vh'}
      justify={'space-around'}
      align={'center'}
    >
      <Navbar />
      <SideNav />
      <Flex
        flexDir={'column'}
        h={'50%'}
        justify={'center'}
        align='center'
        ml={'15vw'}
      >
        <Flex>
          <PriceForm value={priceValue} handleChange={handlePriceChange} />
          <QuantityForm value={quantityValue} handleChange={handleQuantityChange} />
        </Flex>
        <Button
          colorScheme={'teal'}
          onClick={handleSubmit}
          mt={5}
          minW={'15vw'}
          maxW={'15vw'}
        >Save</Button>
      </Flex>
    </Flex >
  )
}

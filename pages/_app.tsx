import "@fontsource/ubuntu";
import "../styles/globals.css";
import type { AppProps } from "next/app";
import theme from "../styles/theme";
import { ChakraProvider } from "@chakra-ui/react";
import { UserProvider } from "../context/user-context";
import { PhotoProvider } from "../context/photo-context";
import { CustomerProvider } from '../context/customer-context';
import { ItemProvider } from '../context/item-context';



function MyApp({ Component, pageProps }: AppProps) {
  return (
    <ChakraProvider theme={theme}>
      <UserProvider>
        <PhotoProvider>
          <CustomerProvider>
            <ItemProvider>
              <Component {...pageProps} />
            </ItemProvider>
          </CustomerProvider>
        </PhotoProvider>
      </UserProvider>
    </ChakraProvider>
  );
}

export default MyApp;

import type { NextPage } from "next";
import { Flex, Heading } from "@chakra-ui/react";
import Navbar from "../components/navbar";

const Home: NextPage = () => {
  return (
    <Flex
      m="0"
      className="index-body"
      h="100vh"
      flexDir="column"
      justify="center"
      align="center"
    >
      <Navbar />
      <Heading as="h1" fontSize="5xl" fontFamily="Ubuntu">
        Order Me Photo
      </Heading>
      <Heading fontSize="2xl"> Login to start session</Heading>
    </Flex>
  );
};

export default Home;

import { useContext } from 'react';
import { useRouter } from 'next/router';
import { Box, Button, Flex, Heading } from '@chakra-ui/react';
import useCart from '../state/cart.state';
import { CustomerContext } from '../context/customer-context';
import { IItem } from '../models/IItem';

export default function Checkout() {
  const { cart } = useCart();
  const { customerState, customerDispatch } = useContext(CustomerContext);
  const router = useRouter()

  function finalizeSession() {
    customerDispatch({ type: 'CLEAR_CUSTOMER' })
    router.push('/customer')
  }

  window.onbeforeprint = (event) => {
    const buttons = document.querySelector('.button-holder')
    buttons.replaceChildren()
  }

  window.onafterprint = (event) => {
    finalizeSession()
  }


  function printPage() {
    window.print()
  }

  return (
    <Flex h='90vh' flexDir='column' justify='center' align='center'>
      <Heading>
        Order Summery
      </Heading>

      <Box mt={5}> Customer: {customerState.user.Name}</Box>
      <Box mt={5} mb={3}>
        Items:
      </Box>
      {cart.map((item: IItem) => {
        return item.Name
      })}
      <Flex mt={5} className="button-holder" flexDir={'row'} justify={'space-around'} minW={'10vw'}>
        <Button colorScheme={'purple'} onClick={() => { printPage() }}>Print</Button>
        <Button colorScheme={'teal'} onClick={() => { finalizeSession() }} >Finish</Button>
      </Flex>
    </Flex >
  )
}

import { Flex, Heading } from "@chakra-ui/react"
import useSWR from 'swr'
import CustomerList from "../components/customer-list"
import Navbar from "../components/navbar"
import SideNav from "../components/side-nav"
import { Global } from "../constants/constants"

const fetcher = (url) => fetch(url).then((res) => res.json())

const API = `${Global.API_URL}/customers/`

export default function CurrentCustomers() {
  const { data, error, mutate } = useSWR(API, fetcher)

  return (
    <Flex
      flexDir={'column'}
      h={'93vh'}
      justify={'space-around'}
      align={'center'}
    >
      <Navbar />
      <SideNav />
      <CustomerList customer={data} onDelete={mutate} />
    </Flex>
  )
}

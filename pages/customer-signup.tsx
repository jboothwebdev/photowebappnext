import { Button, Flex, FormControl, FormLabel, FormErrorMessage, Input } from '@chakra-ui/react';
import useForm from '../hooks/useForm';
import { useRouter } from 'next/router';
import { CustomerContext } from '../context/customer-context';
import { useContext } from 'react';
import { customerService } from '../services/customer-service';

const CustomerSignup = (props: any) => {
  const { values, handleChange, handleSubmit } = useForm(() => {
    saveCustomerToState();
  })
  const { customerState, customerDispatch } = useContext(CustomerContext)

  const router = useRouter();

  const saveCustomerToState = () => {
    let payload = {
      Name: values.name,
      Email: values.email,
      PhoneNumber: values.phonenumber,
      Studio: values.studio
    }
    customerService.prepareCustomer(payload)
    customerDispatch({ type: "SET_CUSTOMER", payload: payload })
    props.changeCustomerSignedUp();
  }

  return (
    <Flex
      position='absolute'
      top={'5vh'}
      minH={'90vh'}
      height={'100%'}
      width={'100%'}
      justify='center'
      align='center'
    >
      <form onSubmit={handleSubmit}>
        <FormControl mt={2} isRequired>
          <FormLabel>Name</FormLabel>
          <Input id='name' type='name' name='name' value={values.name} onChange={handleChange} />
          <FormErrorMessage> Name is required</FormErrorMessage>
        </FormControl>
        <FormControl mt={2} isRequired>
          <FormLabel>Email</FormLabel>
          <Input id='email' type='email' name='email' value={values.email} onChange={handleChange} />
          <FormErrorMessage> Email is required</FormErrorMessage>
        </FormControl>
        <FormControl mt={2} isRequired>
          <FormLabel>Phonenumber</FormLabel>
          <Input id='phonenumber' type='phonenumber' name='phonenumber' value={values.phonenumber} onChange={handleChange} />
          <FormErrorMessage> Phonenumber is required</FormErrorMessage>
        </FormControl>
        <FormControl mt={2} isRequired>
          <FormLabel>Dance Studio</FormLabel>
          <Input id='studio' type='studio' name='studio' value={values.studio} onChange={handleChange} />
          <FormErrorMessage> A studio is required</FormErrorMessage>
        </FormControl>
        <Flex
          mt={2}
          flexDir='row'
          justify='space-around'
        >
          <Button colorScheme='teal' type='submit'>Submit</Button>
          <Button colorScheme='pink' type='button' onClick={() => { router.push('/') }}>Cancel</Button>
        </Flex>
      </form>
    </Flex>
  )
}

export default CustomerSignup;

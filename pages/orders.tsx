import { Flex } from '@chakra-ui/react';
import useSWR from 'swr'
import Navbar from '../components/navbar';
import Orderlist from '../components/orders-list';
import SideNav from '../components/side-nav';
import { Global } from '../constants/constants'

const fetcher = (url) => fetch(url).then((res) => res.json());
const API = "http://localhost:8000/api/items/";

export default function Orders() {
  const { data, error, mutate } = useSWR(API, fetcher);
  return (
    <Flex
      flexDir={'column'}
      h={'93vh'}
      justify={'space-around'}
      align={'center'}
    >
      <Navbar />
      <SideNav />
      <Flex>
        <Orderlist orders={data} sendDelete={mutate} />
      </Flex>
    </Flex>
  )
}


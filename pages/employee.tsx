import type { NextPage } from "next";
import { useContext, useEffect, useState } from "react";
import { UserContext } from "../context/user-context";
import usePrices from "../state/prices.state";
import { Alert, AlertIcon, AlertTitle, AlertDescription, Button, Flex, Heading } from "@chakra-ui/react";
import Navbar from "../components/navbar";
import SideNav from "../components/side-nav";
import { NotAuthorized } from "../components/notAuthorized";

const Employee: NextPage = () => {
  const { state, dispatch } = useContext(UserContext);
  const { prices } = usePrices();
  const [currentView, setCurrentView] = useState("landing")
  const [pricesConfig, setPricesConfig] = useState(false);

  useEffect(() => {
    checkForPrices();
  }, []);

  function checkForPrices() {
    if (prices.print === 0 || prices.digital === 0 || prices.combo === 0) {
      setPricesConfig(false);
    } else {
      setPricesConfig(true)
    }
  }
  const isAuthorized = state.user !== "" ? true : false;

  return (
    <Flex h="100vh" justify="center" align="center">
      {isAuthorized ? (
        <Flex w='100%' justify='center'>
          <Navbar />
          <SideNav />
          {currentView === 'landing' &&
            <Flex flexDir='column' w='50%' ml={'10%'}>
              <Heading> Welcome {state.user}</Heading>
              {!pricesConfig &&
                <Alert status='warning'>
                  <AlertIcon />
                  <AlertTitle>
                    Prices missing:
                  </AlertTitle>
                  <AlertDescription>
                    Some prices are missing please check your selection configuration.
                  </AlertDescription>
                </Alert>
              }
            </Flex>
          }
        </Flex>
      ) : (
        <NotAuthorized />
      )
      }
    </Flex >
  );
};

export default Employee; 

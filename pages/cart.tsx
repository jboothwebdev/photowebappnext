import { Flex, Heading, useColorModeValue } from '@chakra-ui/react';
import useCart from '../state/cart.state';
import CartItem from '../components/cart-item';
import CheckoutPreview from "../components/checkout-preview";
import CustomerNavbar from "../components/cust-navbar";
import { IItem } from "../models/IItem";


const Cart = () => {
  const { cart, setCart } = useCart()
  const bg = useColorModeValue('gray.50', 'teal.50');
  const color = useColorModeValue('gray.700', 'gray.700');

  function deleteCartItem(item: IItem) {
    let filteredItems = cart.filter((cartItem) => {
      return cartItem.Name !== item.Name
    })
    setCart(filteredItems)
  }

  return (
    <Flex
      h={'100vh'}
      w={'100%'}
      flexDir={'row'}
      justify="center"
      align="center"
      className={'cart-outer'}
      borderRadius={'md'}>
      < CustomerNavbar/>
      <Flex
        flexDir={'column'}
        justify={'flex-start'}
        bg={bg}
        color={color}
        boxShadow='dark-lg'
        borderRadius={'md'}
        minH={'60%'}
        minW={'60%'}
      >
        <Heading color={color} m={4}>
          Session Cart
        </Heading>
        <Flex flexDir={'column'} mr={3} ml={3} mb={2}>
          {cart.map((item: any, i: number) => {
            return <CartItem key={i} item={item} deleteItem={deleteCartItem} />
          })}
        </Flex>
      </Flex>
      <CheckoutPreview />
    </Flex>
  )
}

export default Cart;


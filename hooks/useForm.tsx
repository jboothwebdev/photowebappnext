import { ChangeEvent, useState } from "react";

const useForm = (callback: Function) => {
  // set the basic state of the hook to be a empty object
  const [values, setValues] = useState<any>({});

  /**
   * handles the changes to the imputs in the form and updats the state
   * event.target.name is the name of the import
   * value is what is typed in the form.
   */
  const handleChange = (event: ChangeEvent) => {
    event.persist();
    setValues((values: any) => ({
      ...values,
      //@ts-ignore
      [event.target.name]: event.target.value,
    }));
  };

  /**
   * fires a callback function when the form is submitted
   * will be passed where the useState number normally goes
   */
  const handleSubmit = (event: any) => {
    event.preventDefault();
    callback();
  };

  return { values, handleChange, handleSubmit };
};

export default useForm;

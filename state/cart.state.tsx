import useSWR from 'swr'
const initial: any = [];

const useCart = () => {
  const { data, error, mutate } = useSWR('getCart', initial)

  const cart = data || [];
  const isPending = !data
  const setCart = mutate
  return {
    cart,
    setCart,
    error,
    isPending
  }
}

export default useCart; 

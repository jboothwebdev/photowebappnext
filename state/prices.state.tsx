import useSWR from 'swr';
import { IPrices } from '../models/IPrices'

const initial: IPrices = {
  print: 0,
  digital: 0,
  combo: 0
};

const usePrices = () => {
  const { data, error, mutate } = useSWR('getPrices')
  const prices: IPrices = data || initial;
  const isPending = !data
  const setPrices = mutate
  return {
    prices,
    setPrices,
    error,
    isPending
  }
}

export default usePrices;

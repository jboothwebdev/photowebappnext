import useSWR from 'swr';

const initial: number = 1

const useQuantity = () => {
  const { data, error, mutate } = useSWR('getQuantity')
  const quantity: number = data || initial;
  const setQuantity = mutate
  return {
    quantity,
    setQuantity,
    error
  }
}

export default useQuantity;

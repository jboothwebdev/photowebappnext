import { extendTheme } from "@chakra-ui/react";
import { mode } from "@chakra-ui/theme-tools";

const theme = extendTheme({
  config: {
    initialColorMode: "dark",
    useSystemColorMode: false,
  },
  styles: {
    global: (props: any) => ({
      body: {
        fontFamily: "Ubuntu, sans-serif",
        color: mode("gray.800", "blue.50")(props),
        bg: mode("gray.50", "cyan.900")(props),
      },
      fonts: {
        heading: "Ubuntu, sans-serif",
        body: "Ubuntu, sans-serif",
      },
    }),
  },
});

export default theme;

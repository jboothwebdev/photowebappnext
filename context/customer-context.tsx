import React, { createContext, useReducer } from 'react';

type AppState = typeof initialState;

type Action =
  | { type: "SET_CUSTOMER"; payload: any }
  | { type: "CLEAR_CUSTOMER" };

interface CusotmerProviderProps {
  children: React.ReactNode;
}

const initialState = {
  user: {
    name: '',
    email: '',
    phoneNumber: '',
    studio: ''
  }
}

const reducer = (state: AppState, action: Action) => {
  switch (action.type) {
    case "SET_CUSTOMER":
      return {
        ...state,
        user: action.payload,
      };
    case "CLEAR_CUSTOMER":
      return {
        user: {
          name: '',
          email: '',
          phoneNumber: '',
          studio: ''
        }
      }
  }
}

const CustomerContext = createContext<{
  customerState: AppState;
  customerDispatch: React.Dispatch<Action>;
}>({ customerState: initialState, customerDispatch: () => { } });

function CustomerProvider({ children }: CusotmerProviderProps) {
  const [customerState, customerDispatch] = useReducer(reducer, initialState);

  return (
    <CustomerContext.Provider value={{ customerState, customerDispatch }}>
      {children}
    </CustomerContext.Provider>
  )
}

export { CustomerContext, CustomerProvider };

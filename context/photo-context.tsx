import React, { createContext, useReducer } from "react";

type AppState = typeof initialState;

type Action =
  | { type: "SET_PHOTO"; payload: Array<any> | any }
  | {
    type: "CLEAR_PHOTO";
  };

interface PhotoProviderProps {
  children: React.ReactNode;
}

const initialState = {
  photos: [],
};

const reducer = (state: AppState, action: Action) => {
  switch (action.type) {
    case "SET_PHOTO":
      return {
        ...state,
        photos: action.payload,
      };
    case "CLEAR_PHOTO":
      return {
        ...state,
        photo: [],
      };
  }
};

const PhotoContext = createContext<{
  photoState: AppState;
  photoDispatch: React.Dispatch<Action>;
}>({ photoState: initialState, photoDispatch: () => { } });

function PhotoProvider({ children }: PhotoProviderProps) {
  const [photoState, photoDispatch] = useReducer(reducer, initialState);

  return (
    <PhotoContext.Provider value={{ photoState, photoDispatch }}>
      {children}
    </PhotoContext.Provider>
  );
}
export { PhotoContext, PhotoProvider };

import React, { createContext, useReducer } from 'react';

type AppState = typeof initialState;

type Action = | { type: "SET_ITEM"; payload: Array<any> | any }
  | { type: "CLEAR_ITEM"; }

interface ItemProviderProps {
  children: React.ReactNode;
}

const initialState = {
  items: [],
}

const reducer = (state: AppState, action: Action) => {
  switch (action.type) {
    case "SET_ITEM":
      return {
        ...state,
        items: action.payload
      };
    case "CLEAR_ITEM":
      return {
        ...state,
        items: [],
      };
  }
};

const ItemContext = createContext<{
  itemState: AppState;
  itemDispatch: React.Dispatch<Action>;
}>({ itemState: initialState, itemDispatch: () => { } });

function ItemProvider({ children }: ItemProviderProps) {
  const [itemState, itemDispatch] = useReducer(reducer, initialState);
  return (
    <ItemContext.Provider value={{ itemState, itemDispatch }}>
      {children}
    </ItemContext.Provider>
  )
};

export { ItemContext, ItemProvider }

import React, { createContext, useReducer } from "react";

/**
 * Because we are using type script to make this work we have to define the types
 * AppState micks the initialState
 */
type AppState = typeof initialState;

/**
 * Action types are difient here to add must use the | operator;
 *
 */
type Action =
  | { type: "SET_USER"; payload: string }
  | {
      type: "CLEAR_USER";
    };

// Interface for the provider to know about the children

interface UserProviderProps {
  children: React.ReactNode;
}

const initialState = {
  user: "",
};

const reducer = (state: AppState, action: Action) => {
  switch (action.type) {
    case "SET_USER":
      return {
        ...state,
        user: action.payload,
      };
    case "CLEAR_USER":
      return {
        ...state,
        user: "",
      };
    default:
      return state;
  }
};

/**
 * Creating the Context or 'store' for typescript we have to pass in the types
 * In the react dispatch we pass in the types for the actions
 */
const UserContext = createContext<{
  state: AppState;
  dispatch: React.Dispatch<Action>;
}>({ state: initialState, dispatch: () => {} });

function UserProvider({ children }: UserProviderProps) {
  const [state, dispatch] = useReducer(reducer, initialState);

  return (
    <UserContext.Provider value={{ state, dispatch }}>
      {children}
    </UserContext.Provider>
  );
}

export { UserProvider, UserContext };

export interface IEncodedPhoto {
  name: string,
  data: string
}

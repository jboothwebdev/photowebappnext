export interface IItem {
  id?: number,
  Name: string,
  Quantity: number,
  Medium: string,
  CustomerEmail: string,
  Price?: number
}

export interface IPrices {
  digital: number
  print: number
  combo: number
}

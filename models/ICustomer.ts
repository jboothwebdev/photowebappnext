export interface ICustomer {
  id?: number;
  Name: string;
  Email: string;
  PhoneNumber: string;
  Studio: string;
}

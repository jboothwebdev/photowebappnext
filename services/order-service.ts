import axios from "axios"
import { Global } from '../constants/constants';
import { IItem } from '../models/IItem'

async function prepareOrder(body: any) {

  async function sendOrder(body: any) {
    try {
      const response = await axios.post(`${Global.API_URL}/items/`, body);
      return response.data
    } catch (error) {
      return error
    }

  };
  return sendOrder(body)
};

async function getOrders() {
  let response = {}
  await axios.get(`${Global.API_URL}/items/`).then((res) => response = res.data)
  return response
}

async function updateOrder(body: IItem, id: string | any) {
  let response = {}
  fetch(`${Global.API_URL}/items/${id}`, {
    method: 'put',
    headers: { 'Content-Type': "application/json" },
    body: JSON.stringify(body)
  }).then((res) => response = res.json())
  return response
}

async function deleteOrder(item: IItem) {
  let response = {}
  fetch(
    `${Global.API_URL}/items/`,
    {
      method: 'delete',
      headers: { 'Content-Type': "application/json" },
      body: JSON.stringify(item)
    })
    .then((res) => response = res.json())
  return response
}

export const orderService = { prepareOrder, getOrders, deleteOrder, updateOrder }


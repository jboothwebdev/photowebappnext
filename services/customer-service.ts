import axios from 'axios';
import { Global } from '../constants/constants';
import { ICustomer } from '../models/ICustomer';
import { IItem } from '../models/IItem';

async function prepareCustomer(body: ICustomer) {
  async function sendCustomer(body: ICustomer) {
    try {
      const response = await axios.post(`${Global.API_URL}/customers/`, body)
      return response.data;
    } catch (error) {
      return error
    }
  }
  return sendCustomer(body)
}

function getItemsByCustomer(body: any): Array<IItem> {
  function sendRequest(body: any): Array<IItem> {
    try {
      let response = []
      axios.post(`${Global.API_URL}/customers-items`, body).then(res => response = res.data)
      return response
    } catch (error) {
      return error
    }
  }
  return sendRequest(body)
}

async function deleteCustomer(customer: ICustomer) {
  let response = {}
  fetch(
    `${Global.API_URL}/customer-id`, {
    method: 'delete',
    headers: { 'Content-Type': "application/json" },
    body: JSON.stringify(customer)
  })
    .then(
      (res) => response = res
    )
  return response
}



export const customerService = { prepareCustomer, getItemsByCustomer, deleteCustomer };

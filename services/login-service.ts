import axios from "axios";
import { Iemployee } from "../models/IEmployee";
import { Global } from "../constants/constants";

const getEmployee: string | any = async (
  username: string,
  password: string
) => {
  const body: Iemployee = {
    Username: username,
    Password: password,
  };

  const sendRequest = async (body: any) => {
    try {
      const response = await axios.post(`${Global.API_URL}/login`, body);
      return response.data;
    } catch (error) {
      return error;
    }
  };
  return sendRequest(body);
};

export const LoginService = {
  getEmployee,
};

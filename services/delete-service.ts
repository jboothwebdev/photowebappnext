import { Global } from '../constants/constants';


async function deleteAll() {
  try {
    await fetch(`${Global.API_URL}/delete-all`, {
      method: 'delete'
    })
  } catch (error) {
    return error
  }
}

export const deleteService = { deleteAll } 

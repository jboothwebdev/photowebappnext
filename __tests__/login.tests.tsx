import Login from "../pages/login";
import { render, screen } from "@testing-library/react";

/**
 * @jest-environment jsdom
 */

describe("Login page ", () => {
  /**
   * @jest-environment jsdom
   */
  it("Should render form on load", () => {
    render(<Login />);

    const form = screen.getByRole("textbox", {
      name: "Name",
    });

    expect(form).toBeTruthy();
  });
});

import { render, screen, fireEvent } from "@testing-library/react";
import PhotoSelectionForm from '../components/photo-selection-form';

describe('PhotoSelectionForm', () => {

  const setup = () => {
    render(<PhotoSelectionForm />)
  }

  it('Should render the for selection form', () => {
    setup()
  })

  it('Should have a quanty input', () => {
    setup()
    const quanty = screen.getByText('Quantity')
    expect(quanty).toBeTruthy();
  })

  it('Should have a format input', () => {
    setup()
    const format = screen.getByText('Format')
    expect(format).toBeTruthy();
  })

  it('Should Close the modal when canceled', () => {

    const onClose = jest.fn()
    render(<PhotoSelectionForm sendOnClose={onClose} />)
    const cancel = screen.getByText('Cancel')
    fireEvent.click(cancel)
    expect(onClose).toHaveBeenCalled();
  })
})

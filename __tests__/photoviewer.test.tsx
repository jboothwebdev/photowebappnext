import PhotoViewer from "../components/photo-viewer";
import { render, screen, fireEvent } from '@testing-library/react';

describe('Photoviewer page', () => {
  const photo = [{ name: 'test', data: '1' }]
  it("should render the photo-viewer Page", () => {
    render(<PhotoViewer unformatedPhotos={photo} />);
  })

  it("Should render the photo", () => {
    render(<PhotoViewer unformatedPhotos={photo} />);

    const renderedPhoto = screen.getByAltText('test');

    expect(renderedPhoto).toBeTruthy();
  })

  it("Should open the modal when clicking on a photo", async () => {
    const component = render(<PhotoViewer unformatedPhotos={photo} />);
    const target = component.getByAltText('test');
    let totalPhoto: any = [];

    fireEvent.click(target);
    const modal = screen.getByTestId('photoModal');
    totalPhoto = screen.getAllByAltText('test')
    expect(modal).toBeTruthy;
    setTimeout(() => {
      expect(totalPhoto.length).toBe(2)
    }, 200)
  })
})



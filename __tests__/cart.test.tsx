import Cart from '../pages/cart';
import { render, screen, fireEvent } from '@testing-library/react';

describe('Cart page', () => {
  const setup = () => {
    render(<Cart />)
  }
  it("Should render the cart", () => {
    setup();
    const cartText = screen.getByText("Session Cart")
    expect(cartText).toBeTruthy();
  })
})

import Employee from "../pages/employee";

import { render, screen } from "@testing-library/react";

describe("Employee Page ", () => {
  it("Should render the Employee Page", () => {
    render(<Employee />);
  });

  it("Should not render the Welcome", () => {
    render(<Employee />);
    const welcome = screen.getByText('401: Not Authorized')
    expect(welcome).toBeTruthy()
  })
});

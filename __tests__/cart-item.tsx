import CartItem from '../components/cart-item';
import { render, screen } from '@testing-library/react';

describe('Cart Item', () => {
  const itemName = "test"
  const itemQuantity = 1
  const itemPrice = 5


  it('Should create the cart item', () => {
    expect(render(<CartItem />)).toBeTruthy();
  })

  it("Should render a items name", () => {
    render(<CartItem itemName={itemName} />)

    const item = screen.getByText('test');
    expect(item).toBeTruthy();
  })

  it('Should render a items quantity', () => {
    render(<CartItem itemQuantity={itemQuantity} />)

    const item = screen.getByText('1')
    expect(item).toBeTruthy();
  })

  it("Should render a item's price", () => {
    render(<CartItem itemPrice={itemPrice} />)

    const item = screen.getByText('5');
    expect(item).toBeTruthy();
  })
})
